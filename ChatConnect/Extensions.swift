//
//  Extensions.swift
//  ChatConnect
//
//  Created by Visakeswaran N on 29/10/17.
//  Copyright © 2017 Visakeswaran N. All rights reserved.
//

import UIKit

let imageCache = NSCache<AnyObject, AnyObject>()

extension UIImageView {
    
    func loadImageUsingCacheWithUrlString(urlString: String){
        
                self.image = nil
                    // Check for image in the cacge
                if let cachedImage = imageCache.object(forKey: urlString as AnyObject) as? UIImage{
                        self.image = cachedImage
                        return
                    }
        
                    //Otherwise fire off a new download
                    let url = URL(string: urlString)
        
                    URLSession.shared.dataTask(with: url!, completionHandler: { (data, response, error) in
                        if error != nil{
                            print(error!.localizedDescription)
                            return
                        }
        
                        DispatchQueue.main.async(execute: {
                            
                            //image Cache
                            if let downloadedImage = UIImage(data: data!){
                                imageCache.setObject(downloadedImage, forKey: urlString as AnyObject)
                                self.image = downloadedImage
                            }
                            
                            
                        })
        
        
                    }).resume()
        
    }
    
}
