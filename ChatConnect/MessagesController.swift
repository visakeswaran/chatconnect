//
//  ViewController.swift
//  ChatConnect
//
//  Created by Visakeswaran N on 09/09/17.
//  Copyright © 2017 Visakeswaran N. All rights reserved.
//

import UIKit
import Firebase

class MessagesController: UITableViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        //logout button
        navigationItem.leftBarButtonItem = UIBarButtonItem(title: "Logout", style: .plain, target: self, action: #selector(handelLogout))
        
        
        //create a new message icon in nav bar
        let image = UIImage(named: "new_message_icon")
        navigationItem.rightBarButtonItem = UIBarButtonItem(image: image, style: .plain, target: self, action: #selector(handelNewMessage))
        
        checkUserLoggedIn()
        
    }
    
    
    //hadel new message icon press
    @objc func handelNewMessage(){
        let newMessageController = NewMessageController()
        let navController = UINavigationController(rootViewController: newMessageController)
        present(navController, animated: true, completion: nil)
    }
    
    
    //check user logged in
    @objc func checkUserLoggedIn(){
        //Firebase command to check the authentication status
        if Auth.auth().currentUser?.uid == nil{
            perform(#selector(handelLogout), with: nil, afterDelay: 0)
        }else{
            
            fetchUserAddSetupNavBarTitle() 
           
        }
    }
    
    
    func fetchUserAddSetupNavBarTitle() {
        //fetch a single event type/sigle user
        guard let uid = Auth.auth().currentUser?.uid else{
            //if for some reason the uid is nil
            return
        }
        Database.database().reference().child("users").child(uid).observe(.value, with: { (snapshot) in
            
            //set the navigation bar title to the username
            if let dict = snapshot.value as? [String: AnyObject]{
                self.navigationItem.title = dict["name"] as? String
            }
            
        }, withCancel: nil)
    }

    //function evoked when logout button pressed
    @objc func handelLogout() {
        
        do{
            //Firebase command for logout
            try Auth.auth().signOut()
        }catch let logoutErr{
            print(logoutErr)
        }
        
        let loginController = LoginController()
        present(loginController, animated: true, completion: nil)
        
    }

}

