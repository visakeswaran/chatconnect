//
//  LoginController+handlers.swift
//  ChatConnect
//
//  Created by Visakeswaran N on 25/10/17.
//  Copyright © 2017 Visakeswaran N. All rights reserved.
//

import UIKit
import Firebase

extension LoginController: UIImagePickerControllerDelegate, UINavigationControllerDelegate{
    
    
    
    //segmented controll choice handeler
    @objc func handelLoginRegister() {
        if loginRegisterSegmentedControl.selectedSegmentIndex == 0 {
            handelLogin()
        }else{
            handelRegisterButtonClick()
        }
    }
    
    
    
    
    
    //handel LOGIN if on login segment
    @objc func handelLogin() {
        
        //guard function to keep the entered info safe while handeling the option unwrapping
        guard let email = emailTextField.text, let password = passwordTextField.text else {
            print("Form is not valid")
            return
        }
        
        //Firebase command to sign in with the entered details
        Auth.auth().signIn(withEmail: email, password: password) { (user, error) in
            if error != nil{
                print(error!.localizedDescription)
                return
            }
            
            //successfully logged in user
            self.dismiss(animated: true, completion: nil )
        }
    }
    
    
    
    
    
    //handel REGISTER if on register segment
    @objc func handelRegisterButtonClick() {
        //Handel the Register Authentication in here
        
        //guard function to keep the entered info safe while handeling the option unwrapping
        guard let email = emailTextField.text, let password = passwordTextField.text, let name = nameTextField.text else {
            print("Form is not valid")
            return
        }
        
        
        //Firebase Command to regitser a new user
        Auth.auth().createUser(withEmail: email, password: password) { (user, error) in
            if error != nil{
                print(error!.localizedDescription)
            }
            
            guard let uid = user?.uid else{
                return
            }
            
            //successfully authenticated user
            
            
            //Store the imgae into firebase storage
            
            //we need unique name for the images we upload
            let imageName = NSUUID().uuidString
            
            let storageRef = Storage.storage().reference().child("profile_images").child("\(imageName).png")
            if let uploadData = UIImagePNGRepresentation(self.profileImageView.image!){
                storageRef.putData(uploadData, metadata: nil, completion: { (metadata, error) in
                    if error != nil{
                        print(error!)
                    }
                    
                    if let profileImageURL = metadata?.downloadURL()?.absoluteString{
                        
                        let values = ["name": name, "email": email, "profileImageURL": profileImageURL]
                    
                        //Now we have to persist the users we are craeting into the database
                        
                        self.registerUserIntoDatabaseWithUID(uid: uid, values: values)
                    }
                })
            }
        }
    }
    
    
    
    
    
    private func registerUserIntoDatabaseWithUID(uid: String, values: [String: Any]) {
        
        //Firebase databse regerence URL
        let ref = Database.database().reference(fromURL: "https://chatconnect-9c3d5.firebaseio.com/")
        
        //creating the databse hirearchy
        let usersReference = ref.child("users").child(uid)
        
        
        //Firebase command to add the users to the Databse
        usersReference.updateChildValues(values, withCompletionBlock: { (err, ref) in
            
            if let error = err{
                print(error)
                return
            }
            
            self.msgsController?.fetchUserAddSetupNavBarTitle()
            self.dismiss(animated: true, completion: nil)
            
        })
        
    }
    
    
    
    //Handel the tap gesture on profile image to add a new profile image
    @objc func handelProfileImageViewTap() {
        
        let picker = UIImagePickerController()
        
        picker.delegate = self
        picker.allowsEditing = true
        
        present(picker, animated: true, completion: nil)
        
    }
    
    
    
    //Hadel when we select an image from picker
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        
        var selectedImageFromPicker: UIImage?
        
        if let editedImage = info["UIImagePickerControllerEditedImage"] as? UIImage{
            selectedImageFromPicker = editedImage
        }else if let originalImage = info["UIImagePickerControllerOriginalImage"] as? UIImage{
            selectedImageFromPicker = originalImage
        }
        
        if let selectedImage = selectedImageFromPicker{
            profileImageView.image = selectedImage
        }
            
        dismiss(animated: true, completion: nil)
    }
    
    
    //handel what hapens when cancel on image picker is clicked
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
    
    
    
    
    //handel login register toggle
    @objc func handelLoginRegisterToggle() {
        let title = loginRegisterSegmentedControl.titleForSegment(at: loginRegisterSegmentedControl.selectedSegmentIndex)
        loginRegisterButton.setTitle(title, for: .normal )
        
        //change height of input container
        inputContainerViewHeightAnchor?.constant = loginRegisterSegmentedControl.selectedSegmentIndex == 0 ? 100 : 150
        
        //change height of text fields according to the segmentcontroll value
        nameTextFieldHeightConstraint?.isActive = false
        nameTextFieldHeightConstraint = nameTextField.heightAnchor.constraint(equalTo: inputContainerView.heightAnchor, multiplier: loginRegisterSegmentedControl.selectedSegmentIndex == 0 ? 0 : 1/3)
        nameTextFieldHeightConstraint?.isActive = true
        
        emailTextFieldHeightConstraint?.isActive = false
        emailTextFieldHeightConstraint = emailTextField.heightAnchor.constraint(equalTo: inputContainerView.heightAnchor, multiplier: loginRegisterSegmentedControl.selectedSegmentIndex == 0 ? 1/2 : 1/3)
        emailTextFieldHeightConstraint?.isActive = true
        
        passwordTextFieldHeightConstraint?.isActive = false
        passwordTextFieldHeightConstraint = passwordTextField.heightAnchor.constraint(equalTo: inputContainerView.heightAnchor, multiplier: loginRegisterSegmentedControl.selectedSegmentIndex == 0 ? 1/2 : 1/3)
        passwordTextFieldHeightConstraint?.isActive = true
    }
    
    
    
}
