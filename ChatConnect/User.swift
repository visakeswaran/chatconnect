//
//  User.swift
//  ChatConnect
//
//  Created by Visakeswaran N on 25/10/17.
//  Copyright © 2017 Visakeswaran N. All rights reserved.
//

import UIKit

class User: NSObject {
    
   @objc var name: String?
   @objc var email: String?
    @objc var profileImageURL: String?
}
