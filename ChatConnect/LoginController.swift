//
//  LoginController.swift
//  ChatConnect
//
//  Created by Visakeswaran N on 09/09/17.
//  Copyright © 2017 Visakeswaran N. All rights reserved.
//

import UIKit
import Firebase

class LoginController: UIViewController {

    var msgsController: MessagesController?
    //DESIGNING THE UI COMPONENTS NEEDED
    
    //INPUT VIEW
    let inputContainerView: UIView = {
        
        let view = UIView()
        
        view.backgroundColor = UIColor.white
        view.translatesAutoresizingMaskIntoConstraints = false
        view.layer.cornerRadius = 5
        view.layer.masksToBounds = true
        
        return view
    }()
    
    
    
    //Login Register Button
    lazy var loginRegisterButton: UIButton = {
        let button = UIButton(type: .system)
        
        button.backgroundColor = UIColor(red: 153/255, green: 63/255, blue: 66/255, alpha: 1)
        button.setTitle("Register", for: .normal)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setTitleColor(UIColor.white, for: .normal)
        button.titleLabel?.font = UIFont.boldSystemFont(ofSize: 16)
        button.layer.cornerRadius = 5
        button.layer.masksToBounds = true
        
        //ButtonClick Event
        button.addTarget(self, action: #selector(handelLoginRegister), for: .touchUpInside)
        
        return button
    }()
    

    
    
    
    //Profile image
    lazy var profileImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.image = UIImage(named: "avatar")
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.contentMode = .scaleAspectFill
        
        //image tap to pick
        let tap = UITapGestureRecognizer(target: self, action: #selector(handelProfileImageViewTap))
        imageView.addGestureRecognizer(tap)
        imageView.isUserInteractionEnabled = true
        
        return imageView
    }()
    
    
    
    
    
    //Segmented control
    lazy var loginRegisterSegmentedControl: UISegmentedControl = {
       let sc = UISegmentedControl(items: ["Login", "Register"])
       sc.translatesAutoresizingMaskIntoConstraints = false
       sc.tintColor = UIColor.white
       sc.selectedSegmentIndex = 1
       sc.addTarget(self, action: #selector(handelLoginRegisterToggle), for: .valueChanged)
       return sc
    }()
    
    
  
    
    
    
    //Name text field
    let nameTextField: UITextField = {
        let tf = UITextField()
        
        tf.placeholder = "Name"
        tf.translatesAutoresizingMaskIntoConstraints = false
        
        return tf
    }()
    
    
    
    //seprator line after name text field
    let nameSepratorView: UIView = {
        let view = UIView()
        
        view.backgroundColor = UIColor(red: 220/255, green: 220/255, blue: 220/255, alpha: 1)
        view.translatesAutoresizingMaskIntoConstraints = false
       
        return view
    }()

    
    
    //Email text field
    let emailTextField: UITextField = {
        let tf = UITextField()
        
        tf.placeholder = "Email Address"
        tf.translatesAutoresizingMaskIntoConstraints = false
        
        return tf
    }()
    
    
    
    
    //seprator line after Email text field
    let emailSepratorView: UIView = {
        let view = UIView()
        
        view.backgroundColor = UIColor(red: 220/255, green: 220/255, blue: 220/255, alpha: 1)
        view.translatesAutoresizingMaskIntoConstraints = false
        
        return view
    }()

    
    
    
    //Password text field
    let passwordTextField: UITextField = {
        let tf = UITextField()
        
        tf.placeholder = "Password"
        tf.translatesAutoresizingMaskIntoConstraints = false
        tf.isSecureTextEntry = true
        
        return tf
    }()
    
    
    
    let headLabel: UILabel = {
        let lbl = UILabel()
        
        lbl.text = "Chatter"
        lbl.translatesAutoresizingMaskIntoConstraints = false
        lbl.textColor = UIColor.white
        lbl.textAlignment = .center
        lbl.font = UIFont(name: "Chalkduster", size: 32)
        
        return lbl
    }()
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        view.backgroundColor = UIColor(red: 255/255, green: 105/255, blue: 60/255, alpha: 1)
        
        //Adding the designed views
        view.addSubview(inputContainerView)
        view.addSubview(loginRegisterButton)
        view.addSubview(headLabel)
        view.addSubview(profileImageView)
        view.addSubview(loginRegisterSegmentedControl)
        
        //Constraint fucnctions for the added subviews
        setupInputsContainer()
        setupLoginRegisterButton()
        setupProfileImageView()
        setupHeadLabel()
        setupLoginRegisterSegmentedControl()
    }
    
    
    
    
    
    
//    <=========    SETTING THE ATTRIBUTES AND CONSTARINTS FOR THE UI   =============>
    
    func setupLoginRegisterSegmentedControl() {
        loginRegisterSegmentedControl.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        loginRegisterSegmentedControl.bottomAnchor.constraint(equalTo: inputContainerView.topAnchor, constant: -12).isActive = true
        loginRegisterSegmentedControl.widthAnchor.constraint(equalTo: inputContainerView.widthAnchor, multiplier: 1)
        loginRegisterSegmentedControl.heightAnchor.constraint(equalToConstant: 36).isActive = true
    }
    
    func setupProfileImageView() {
        profileImageView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        profileImageView.topAnchor.constraint(equalTo: headLabel.bottomAnchor, constant: 12).isActive = true
        profileImageView.bottomAnchor.constraint(equalTo: loginRegisterSegmentedControl.topAnchor, constant: -35).isActive = true
        profileImageView.widthAnchor.constraint(equalToConstant: 100).isActive = true
        profileImageView.heightAnchor.constraint(equalToConstant: 100).isActive = true
    }
    
    func setupHeadLabel(){
        headLabel.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        headLabel.bottomAnchor.constraint(equalTo: profileImageView.topAnchor, constant: -30).isActive = true
        headLabel.topAnchor.constraint(equalTo: view.topAnchor, constant: 60).isActive = true
        headLabel.heightAnchor.constraint(equalToConstant: 50).isActive = true
    }
    
    //holiding reference to some toggle-able constraints
    var inputContainerViewHeightAnchor: NSLayoutConstraint?
    var nameTextFieldHeightConstraint: NSLayoutConstraint?
    var emailTextFieldHeightConstraint: NSLayoutConstraint?
    var passwordTextFieldHeightConstraint: NSLayoutConstraint?
    
    func setupInputsContainer() {
        //Add constraints to INPUT VIEW x, y, w, h
        inputContainerView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        inputContainerView.widthAnchor.constraint(equalTo: view.widthAnchor, constant: -24).isActive = true
        inputContainerView.heightAnchor.constraint(equalToConstant: 150).isActive = true
        inputContainerViewHeightAnchor = inputContainerView.heightAnchor.constraint(equalToConstant: 150)
        inputContainerViewHeightAnchor? .isActive = true
        
        //add the fie ld in here
        inputContainerView.addSubview(nameTextField)
        inputContainerView.addSubview(nameSepratorView)
        inputContainerView.addSubview(emailTextField)
        inputContainerView.addSubview(emailSepratorView)
        inputContainerView.addSubview(passwordTextField)
        
        //Add constraints to Name Text Field x, y, w, h
        nameTextField.leftAnchor.constraint(equalTo: inputContainerView.leftAnchor, constant: 12).isActive = true
        nameTextField.topAnchor.constraint(equalTo: inputContainerView.topAnchor).isActive = true
        nameTextField.widthAnchor.constraint(equalTo: inputContainerView.widthAnchor).isActive = true
        nameTextFieldHeightConstraint = nameTextField.heightAnchor.constraint(equalTo: inputContainerView.heightAnchor, multiplier: 1/3)
        nameTextFieldHeightConstraint? .isActive = true
        
        //Add constraints to Name Seprator View x, y, w, h
        nameSepratorView.leftAnchor.constraint(equalTo: inputContainerView.leftAnchor).isActive = true
        nameSepratorView.topAnchor.constraint(equalTo: nameTextField.bottomAnchor).isActive = true
        nameSepratorView.widthAnchor.constraint(equalTo: inputContainerView.widthAnchor).isActive = true
        nameSepratorView.heightAnchor.constraint(equalToConstant: 1).isActive = true
        
        //Add constraints to Email Text Field x, y, w, h
        emailTextField.leftAnchor.constraint(equalTo: inputContainerView.leftAnchor, constant: 12).isActive = true
        emailTextField.topAnchor.constraint(equalTo: nameSepratorView.topAnchor).isActive = true
        emailTextField.widthAnchor.constraint(equalTo: inputContainerView.widthAnchor).isActive = true
        emailTextFieldHeightConstraint = emailTextField.heightAnchor.constraint(equalTo: inputContainerView.heightAnchor, multiplier: 1/3)
        emailTextFieldHeightConstraint? .isActive = true
        
        //Add constraints to Email Seprator View x, y, w, h
        emailSepratorView.leftAnchor.constraint(equalTo: inputContainerView.leftAnchor).isActive = true
        emailSepratorView.topAnchor.constraint(equalTo: emailTextField.bottomAnchor).isActive = true
        emailSepratorView.widthAnchor.constraint(equalTo: inputContainerView.widthAnchor).isActive = true
        emailSepratorView.heightAnchor.constraint(equalToConstant: 1).isActive = true
        
        //Add constraints to Password Text Field x, y, w, h
        passwordTextField.leftAnchor.constraint(equalTo: inputContainerView.leftAnchor, constant: 12).isActive = true
        passwordTextField.topAnchor.constraint(equalTo: emailSepratorView.topAnchor).isActive = true
        passwordTextField.widthAnchor.constraint(equalTo: inputContainerView.widthAnchor).isActive = true
        passwordTextFieldHeightConstraint = passwordTextField.heightAnchor.constraint(equalTo: inputContainerView.heightAnchor, multiplier: 1/3)
        passwordTextFieldHeightConstraint? .isActive = true
        
    }
    
    func setupLoginRegisterButton(){
        //Add constraints to Login Register Button x, y, w, h
        loginRegisterButton.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        loginRegisterButton.topAnchor.constraint(equalTo: inputContainerView.bottomAnchor, constant: 12).isActive = true
        loginRegisterButton.widthAnchor.constraint(equalTo: inputContainerView.widthAnchor).isActive = true
        loginRegisterButton.heightAnchor.constraint(equalToConstant: 50).isActive = true
    }
    
    //Dismiss the keypad when return is pressed
    
    
    override var preferredStatusBarStyle: UIStatusBarStyle{
        return .lightContent
    }
}
