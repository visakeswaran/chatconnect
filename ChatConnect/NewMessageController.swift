//
//  NewMessageController.swift
//  ChatConnect
//
//  Created by Visakeswaran N on 24/10/17.
//  Copyright © 2017 Visakeswaran N. All rights reserved.
//

import UIKit
import Firebase

class NewMessageController: UITableViewController {

    let cellId = "cellId"
    var users = [User]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //cancel button
        navigationItem.leftBarButtonItem = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(handelCancel))
        
        //registering the newly created table view cell
        tableView.register(UserCell.self, forCellReuseIdentifier: cellId)
        
        
        fetchUsers()
    }
    
    
    
    //fetching all the users from the Firebase database and storng them in an array
    @objc func fetchUsers(){
        
        //Firebase data fetch command
        Database.database().reference().child("users").observe(.childAdded, with: { (snapshot) in
            
            //fetch all the user
            if let dict = snapshot.value as? [String: AnyObject]{
                let user = User()
                user.setValuesForKeys(dict)
                //push into users array
                self.users.append(user)
                
                //reload the table view
                DispatchQueue.main.async {
                    self.tableView.reloadData()
                }
            }
            
        }, withCancel: nil)
        
    }
    
    
    //cancel button handeler
    @objc func handelCancel() {
        dismiss(animated: true, completion: nil)
    }
    

    //table view delegate fucntions
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return users.count
    }

    override  func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 72
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellId, for: indexPath) as! UserCell //downcast as the custom cell
        let user = users[indexPath.row]
        cell.textLabel?.text = user.name
        cell.detailTextLabel?.text = user.email
        
        //download the profile image and display
        if let profileImageUrl = user.profileImageURL{
            
            
            //we nee to cache the image downloaded from code below for efficient network usage
            
            cell.profileImageView.loadImageUsingCacheWithUrlString(urlString: profileImageUrl)
            
//            let url = URL(string: profileImageUrl)
//
//            URLSession.shared.dataTask(with: url!, completionHandler: { (data, response, error) in
//                if error != nil{
//                    print(error!.localizedDescription)
//                    return
//                }
//
//                DispatchQueue.main.async(execute: {
//                    cell.profileImageView.image = UIImage(data: data!)
////                    cell.imageView?.image = UIImage(data: data!)
//                })
//
//
//            }).resume()
            
        }
        
        return cell
    }
}


//custom cell

class UserCell: UITableViewCell {
    
    //overriding the default cell text label
    override func layoutSubviews() {
        super.layoutSubviews()
        
        textLabel?.frame = CGRect(x: 64, y: textLabel!.frame.origin.y - 2, width: textLabel!.frame.width , height: textLabel!.frame.height)
        detailTextLabel?.frame = CGRect(x: 64 , y: detailTextLabel!.frame.origin.y + 2, width: detailTextLabel!.frame.width , height: detailTextLabel!.frame.height)
    }
    
    
    //Add custom image to the cell
    let profileImageView: UIImageView = {
        
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.layer.cornerRadius = 24
        imageView.layer.masksToBounds = true
        imageView.contentMode = .scaleAspectFill
        return imageView
        
    }()
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: .subtitle, reuseIdentifier: reuseIdentifier)
        
        //add the custom image here
        addSubview(profileImageView)
        
        //Constraints to the custom profile image
        profileImageView.leftAnchor.constraint(equalTo: self.leftAnchor, constant: 8).isActive = true
        profileImageView.centerYAnchor.constraint(equalTo: self.centerYAnchor).isActive = true
        profileImageView.widthAnchor.constraint(equalToConstant: 48).isActive = true
        profileImageView.heightAnchor.constraint(equalToConstant: 48).isActive = true
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
